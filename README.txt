WHAT THE PROJECT DOES:

This project  reads a text file containing a complete set of course marks for a group of students, and computes their final overall mark for the course (out of 100) and their corresponding letter grade (A+, A, etc).

The files may also contain bash-style comments (i.e. from a # symbol to the end of the line) which should be ignored by the program.

Other than blank lines and comments, files consist of four information lines followed by a collection of data lines.

The file has a roughly tabular format, where each line consists of a fixed number of alphanumeric fields separated by whitespace.

The four information lines are as follows:

TITLE line
- this line begins with the keyword TITLE, all remaining fields give a unique name for an assessment item, e.g.
	TITLE Lab1 Lab2 Assign1 Midterm Lab3 Lab4 Assign2 FinalExam
CATEGORY line
- this line begins with the keyword CATEGORY, all remaining fields give a category name for the type of assessment item (is it a lab, is it an assignment, etc), e.g.:
	CATEGORY Lab Lab Assignment Midterm Lab Lab Assignment Final
MAXMARK line
- this line begins with the keyword MAXMARK, all remaining fields give the maximum possible mark on that item of assessment, e.g.
	MAXMARK 10 10 15 40 10 10 20 90
WEIGHT line
- this line begins with the keyword WEIGHT, all remaining fields give the value of the item as a percent of the final grade, e.g.
	WEIGHT 5 5 10 20 5 5 10 40
The data lines then consist of one whitespace-delimited line for each student, where the first field is the student username (e.g. wesselsd) and the remaining fields are the student's marks for the assessment items, in the same order as specified by the TITLE line.

Each item's contribution to the final mark (out of 100) can be computed as
(mark * weight) / (maxMark)
Letter grades are assigned by rounding the final mark to the nearest percent, then applying the normal VIU grade scale (as found on the course outline, 90-100 A+, 85-89 A, etc).

REPOSITORY ORGANIZATION:
-----------------------------------------------------
man1 - for the man pages for your grader program and any support tools you create
src - for the C++/bash source code files for your program and tools (it is recommended that you create subdirectories within src to keep the various source code elements organized)
obj - for any .o files created for the "normal" (non-debug) executables
objd - for any .o files created for the debug versions of executables
doc - design, implementation, and test documentation goes here
bin - for any executables created/used by your project
test - for all test files used by your project

DEVELOPER
--------------------------------------------------
shrishti.vasudeva123@gmail.com


