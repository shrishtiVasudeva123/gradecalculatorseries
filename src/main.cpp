#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <fstream>
#include<cmath>
#include <ctime>
#include "student/StudentHelper.h"
using namespace std;
int main(){
   time_t t = time(NULL);
   tm* tPtr = localtime(&t);
   StudentHelper studentHelper;
   string fileToRead;
   cout<<"Enter the filename along with fullpath"<<endl;
   getline(cin,fileToRead,'\n');
    fileToRead = fileToRead+".txt";
   //load and read the grades data file
   bool isFileLoadSuccess = studentHelper.loadFileData(fileToRead);
   if(isFileLoadSuccess){
    cout<<"*************************REPORT*******************************"<<endl;
    cout << "Current Date: " <<(tPtr->tm_mday)<<"/"<< (tPtr->tm_mon)+1 <<"/"<< (tPtr->tm_year)+1900<< endl;
        cout << "Start Time: " << (tPtr->tm_hour)<<":"<< (tPtr->tm_min)<<":"<< (tPtr->tm_sec) << endl;
    studentHelper.displayReport();
    cout<<"**************************************************************";
   }

getch();
return 0;


}
