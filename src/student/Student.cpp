#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <conio.h>
#include <fstream>
#include <string.h>
#include "Student.h"

using namespace std;
void Student::setUsername(std::string p_username){
   username = p_username;
}

void Student::setFinalMarks(float p_finalMarks){
   finalMarks = p_finalMarks;
}

void Student::setGrade(string p_grade){
   grade = p_grade;
}

void Student::print(){
    int spaceCount = 20;
    cout<<username;
    for(int i=1;i<=spaceCount-username.length();i++){
        cout<<" ";
    }
    cout<<finalMarks<<"\t"<<grade<<endl;
}

