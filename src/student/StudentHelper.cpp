#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <conio.h>
#include <fstream>
#include <string.h>
#include "StudentHelper.h"
#include "Student.h"
#include <vector>
#include<map>
using namespace std;


bool StudentHelper :: loadFileData(string filename)
{
     fstream file;
     file.open(filename,ios::in);
if(!file) // Step 3: Checking whether file exist
    {
        cout<<"No such file";
        return false;
    }
    else
    {
        string str;
        bool titleFound = false;
        bool categoryFound = false;
        bool maxmarkFound = false;
        bool weightFound = false;
        while (getline(file,str))
        {
              // Message Read from file

            if (str.find("TITLE") != string::npos) {
                //cout << str << endl;
                titleFound = true;
                list<string> titlesArr = getWordsFromLine(str);
                saveInfoLines(titlesArr,"TITLES");
                //saveTitlesWithWeight(NULL);

        }
            if (str.find("CATEGORY") != string::npos) {
               // cout << str << endl;
                categoryFound = true;
        }
            if (str.find("MAXMARK") != string::npos) {
               // cout << str << endl;
                maxmarkFound = true;

                list<string> maxMarkArr = getWordsFromLine(str);
                saveInfoLines(maxMarkArr,"MAXMARK");


        }
            if (str.find("WEIGHT") != string::npos) {
               // cout << str << endl;
                weightFound = true;

                list<string> weightArr = getWordsFromLine(str);
                saveInfoLines(weightArr,"WEIGHT");
        }

            if(weightFound){
              int blankLineCount = 0;
              while(getline(file,str,'\n')){
                if(!str.empty() && blankLineCount==1){
                   // cout<<str<<endl;
                   createAndSaveStudent(str);
                }
                else{
                    blankLineCount+=1;
                }
              }
        }

        }
        file.close(); // Step 5: Closing file
        return true;
    }

}

void StudentHelper::saveInfoLines(list<string> stringArr,string p_destination){
    if(!stringArr.empty()){

        list <string> :: iterator it;

        if(p_destination.compare("TITLES")==0){
            titlesList.assign(stringArr.begin(),stringArr.end());
        }
        else if(p_destination.compare("MAXMARK")==0){
             list <string> :: iterator it;
             list<string>:: iterator tileIterator;
             tileIterator = titlesList.begin();
            for(it = stringArr.begin(); it != stringArr.end()&&tileIterator!=titlesList.end();){
               titleWithMaxMarkMap.insert({*tileIterator, *it});
                it++;
                tileIterator++;
           }
        }
        else if(p_destination.compare("WEIGHT")==0){
                list <string> :: iterator it;
             list<string>:: iterator tileIterator;
             tileIterator = titlesList.begin();
            for(it = stringArr.begin(); it != stringArr.end()&&tileIterator!=titlesList.end(); ++it,++tileIterator){
               titleWithWeight.insert(pair<string, string>(*tileIterator, *it));
           }
        }
    }
}

std::map<string,string> StudentHelper::getMapTemplate(list<string> wordList){
             map<string,string> templateMap;
             list <string> :: iterator it;
             list<string>:: iterator tileIterator;
             tileIterator = titlesList.begin();
            for(it = wordList.begin(); it != wordList.end()&&tileIterator!=titlesList.end(); ++it,++tileIterator){
               templateMap.insert(pair<string, string>((*tileIterator), (*it)));
           }
           return templateMap;
}

void StudentHelper:: createAndSaveStudent(string str){
   list<string> studentDataList = getWordsFromLine(str);

    map<string,string> studentMarksTemplate = getMapTemplate(studentDataList);
    Student student;
    student.setUsername((studentMarksTemplate.find("TITLE"))->second);
    float result = 0.00;
    result = computeMarks(studentMarksTemplate);
    student.setFinalMarks(result);
    string studentGrade = computeGrades(result);
    student.setGrade(studentGrade);
    addStudentToList(student);

}

float StudentHelper::computeMarks(map<string,string> itemMarksMap){
     list <string> :: iterator it;
     float finalMarks = 0.0;
      bool first = true;
      for(it = titlesList.begin(); it != titlesList.end(); ++it){
        //int num = atoi(itemMarksMap.find(*it)->se);
        if((*it).compare("TITLE")!=0){
            float mark = atof((itemMarksMap.find((*it))->second).c_str());
            int maxMark = atoi((titleWithMaxMarkMap.find(*it)->second).c_str());
            int weight = atoi((titleWithWeight.find(*it)->second).c_str());
            finalMarks += (mark * weight) / (maxMark);
        }

      }
          finalMarks = (int)(finalMarks * 100 + .5);
          return (float)finalMarks/100;
}

string StudentHelper::computeGrades(float finalMarks){

        if(finalMarks>=90.00 && finalMarks<=100.0){
            return "A+";
        }
        else if(finalMarks>=85.00 && finalMarks<=89.99){
            return "A";
        }
        else if(finalMarks>=80.00 && finalMarks<=84.99){
            return "A-";
        }else if(finalMarks>=76.00 && finalMarks<=79.99){
            return "B+";
        }else if(finalMarks>=72.00 && finalMarks<=75.99){
            return "B";
        }else if(finalMarks>=68.00 && finalMarks<=71.99){
            return "B-";
        }else if(finalMarks>=64.00 && finalMarks<=67.99){
            return "C+";
        }else if(finalMarks>=60.00 && finalMarks<=63.99){
            return "C";
        }else if(finalMarks>=55.00 && finalMarks<=59.99){
            return "C-";
        }else if(finalMarks>=50.00 && finalMarks<=54.99){
            return "D";
        }else if(finalMarks>=0.00 && finalMarks<=49.99){
            return "F";
        }
}

void StudentHelper::displayReport(){
     for (auto l_student = studentList.begin(); l_student != studentList.end(); ++l_student)
        (*l_student).print();
}

void StudentHelper::addStudentToList(Student student){
    studentList.push_back(student);
}

list<string> StudentHelper::getWordsFromLine(string str){
   std::stringstream ss(str);
    std::string token;
    list<string> wordsList;
     while (std::getline(ss, token, ' ')){

        wordsList.push_back(token);
       // token = strtok(str, " ");
    }
    return wordsList;
}


/*void StudentHelper :: removeDupWord(string str)
{
   string word = "";
   for (auto x : str)
   {
       if (x == ' ')
       {
           cout << word << endl;
           word = "";
       }
       else
       {
           word = word + x;
       }
   }
   cout << word << endl;
}*/

