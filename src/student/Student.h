#ifndef STUDENT_H_INCLUDED
#define STUDENT_H_INCLUDED
#include<string>
class Student{
private:
      std::string username;
      //int *a = null;
      float finalMarks;
      std::string grade;
public:
    void setUsername(std::string p_username);
    void setFinalMarks(float p_finalMarks);
    void setGrade(std::string p_grade);
    void print();

    std::string getUsername(){ return username;}
    float getFinalMarks(){return finalMarks;}
    std::string getGrade(){return grade;};

};

#endif // STUDENT_H_INCLUDED
