#ifndef STUDENTHELPER_H_INCLUDED
#define STUDENTHELPER_H_INCLUDED
#include<vector>
#include<string>
#include<map>
#include<list>
#include "Student.h"
using namespace std;
class StudentHelper
{
   private:
       std::vector<Student> studentList;
       std:: map<std::string,std::string> titleWithWeight;
        std::map<std::string,std::string> titleWithMaxMarkMap;
        list<string> titlesList;
   public:
       bool loadFileData(string filename);
       float computeMarks(map<string,string> dataMap);
       string computeGrades(float marks);
       void displayReport();
   private:
       void removeDupWord(string str);
       list<string> getWordsFromLine(string str);
       void saveInfoLines(list<string> strlist,string str);
       void createAndSaveStudent(string str);
       map<string,string> getMapTemplate(list<string> wordList);
       void addStudentToList(Student student);
};


#endif // STUDENTHELPER_H_INCLUDED
