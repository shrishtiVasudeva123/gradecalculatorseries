Implementation Details:-

The program execution starts from the main method of "main.cpp". 
At first it need to take the filename with fullpath as input from the user.

In order to provide some modularity in the project, I have created certain header and cpp files.
Therefore the only work of main.cpp is to only call funtions that are needed to be used.

A class 'StudentHelper' acts as a utility class that provides all the necessary functions.
This class is also a helper class for the 'Student' class 
which is basically a Data Transfer Object.

Student class : It will store the each student data which is read from the file like
	-username
	-Final marks
	-Grade
 The getter are implemented in Student.h and Setters and all other methods are implemented in Student.cpp

StudentHelper class has global vector that stores all the Student objects
Some important methods available in this class are:-
 	- loadFileData : reads the file contents line by line. 
			 Looks for the lines containing TITLE,CATEGORY,MAXMARK,WEIGHT
                         and stores them in maps
        - createAndSaveStudent : parse student data line from file and stores the data in a Student object.
        - computeMarks : calculate the final marks as per the formula given.
        - computeGrades : calculates the grade allocated depending on the final Marks.

ERROR Handling is yet to be implemented but this will be done loadFileData method for each Information lines and Student lines

   

To read the contents of the text file load the File

