Purpose:
For testing, various text files having data in different formats has be used in order to 
check whether the validation of the contents is working properly or not.

The input file would be invalid in many ways like:
-if it is incorrectly formatted, 
-contains invalid data, 
-missing fields, 
-extra fields etc.

Test File:
Currently, 'Grades' text file is used to check the grading functionality.
It is a valid file with no error. 

Location:
The test file can be found in 'test/files' repository.

How to test:
Run the program
Enter the filename with fullpath when program prompts.
The program will load the contents into memory and calculate the marks for each student. 
The report will be displayed, if no error occurs. 
   